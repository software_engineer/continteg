<?php

    class UserTest extends \PHPUnit_Framework_TestCase{

        public $user;

        public function setUp(){
            $this->user = new \App\Models\User;
        }
        
        /** @test */
        public function hatWeCanGetTheFirstName(){

            $this->user->setFirstName("Billy");

            $this->assertEquals($this->user->getFirstName(),"Billy");
        }

        public function testThatWeCanGetTheLastName(){

            $this->user->setLastName("Joel");

            $this->assertEquals($this->user->getLastName(),"Joel");
        }

        public function testFullNameIsReturned(){

            $first = $this->user->setFirstName("James");
            $last = $this->user->setLastName("Bond");

            $this->assertEquals($this->user->getFullName(),"James Bond");
        }

        public function testFirstAndLastNameAreTrimmed(){

            $first = $this->user->setFirstName("  James");
            $last = $this->user->setLastName("Bond  ");

            $this->assertEquals($this->user->getFirstName(),"James");
            $this->assertEquals($this->user->getLastName(),"Bond");
        }

        public function testEmailAddressCanBeSet(){

            $this->user->setEmailAddress("james@bond.com");

            $this->assertEquals($this->user->getEmailAddress(),"james@bond.com");
        }

        public function testEmailvariableContainsCorrectValues(){
            $this->user->setFirstName("James");
            $this->user->setLastName("Bond");
            $this->user->setEmailAddress("james@bond.com");

            $emailVariables = $this->user->getEmailVariables();

            $this->arrayHasKey("full_name",$emailVariables);
            $this->arrayHasKey("email",$emailVariables);

            $this->assertEquals($emailVariables["full_name"],"James Bond");
            $this->assertEquals($emailVariables["email"],"james@bond.com");
        }

    }

?>